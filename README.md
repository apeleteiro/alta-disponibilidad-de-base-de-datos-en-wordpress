# Alta Disponibilidad de base de datos en WordPress

Documentación que acompaña al taller [Pon en marcha tu base de datos WordPress en alta disponibilidad](https://galicia.wordcamp.org/2021/session/como-disponer-de-alta-disponibilidad-de-base-de-datos-en-wordpress/) de la WordCamp Galicia 2021.

Para la configuración completa vamos a necesitar 3 servidores:
- 1 servidor web con WordPress instalado.
- 1 servidor para la base de datos principal.
- 1 servidor para la base de datos replicada.

Veamos a continuación todo el proceso de instalación y configuración:

## 1. Instalación de MariaDB en los servidores de base de datos

Primero procedemos a instalar MariaDB en los 2 servidores de base de datos:
```
apt-get install mariadb-server mariadb-client -y
```

Protegemos nuestra instalación de MariaDB:
```
mysql_secure_installation
```

Respondemos a las siguientes preguntas:
```
Enter current password for root (enter for none): Press Enter
Set root password? [Y/n] Y
New password:
Re-enter new password:
Remove anonymous users? [Y/n] Y
Disallow root login remotely? [Y/n] Y
Remove test database and access to it? [Y/n] Y
Reload privilege tables now? [Y/n] Y
```

## 2. Configuración del servidor _Source_ (source.peleteiro.eu)

Editamos el fichero de configuración de MariaDB para habilitar el logging binario:
```
vim /etc/mysql/mariadb.conf.d/50-server.cnf
```

Debemos establecer el parámetro `bind-address` de la siguiente forma:
```
bind-address            = 0.0.0.0
```

Y añadimos al final del fichero de configuración las siguientes líneas:
```
server-id = 1
log_bin = /var/log/mysql/mysql-bin.log
log_bin_index =/var/log/mysql/mysql-bin.log.index
relay_log = /var/log/mysql/mysql-relay-bin
relay_log_index = /var/log/mysql/mysql-relay-bin.index
```

Reiniciamos MariaDB para que los cambios tengan efecto:
```
systemctl restart mariadb
```

Ahora vamos a conectarnos a MariaDB como root para crear los usuarios y establecer la replicación:
```
mysql -u root -p
```

Vamos a crear el usuario para la replicación y el usuario para WordPress:
```
CREATE USER 'replication'@'%' identified by 'your-password';
CREATE USER 'wordpress'@'%' identified by 'your-password';
```

Ahora establecemos permiso de replicación al primero y permisos completos al segundo: 
```
GRANT REPLICATION SLAVE ON *.* TO 'replication'@'%';
GRANT ALL PRIVILEGES ON *.* TO 'wordpress'@'%';
```

Refrescamos los permisos:
```
FLUSH PRIVILEGES;
```

Ahora ejecutaremos el siguiente comando:
```
SHOW MASTER STATUS;
```

Ya que necesitamos los valores **File** y **Position** para establecer en nuestro servidor de réplica:
```
+------------------+----------+--------------+------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+------------------+----------+--------------+------------------+
| mysql-bin.000001 |    1128  |              |                  |
+------------------+----------+--------------+------------------+
```

## 3. Configuración del servidor _Replica_ (replica.peleteiro.eu)

Editamos el fichero de configuración de MariaDB para habilitar el logging binario:
```
vim /etc/mysql/mariadb.conf.d/50-server.cnf
```

Al igual que en _Source_, debemos establecer el parámetro `bind-address` de la siguiente forma:
```
bind-address            = 0.0.0.0
```

Y añadimos al final del fichero de configuración las siguientes líneas:
```
server-id = 2
log_bin = /var/log/mysql/mysql-bin.log
log_bin_index =/var/log/mysql/mysql-bin.log.index
relay_log = /var/log/mysql/mysql-relay-bin
relay_log_index = /var/log/mysql/mysql-relay-bin.index
```

Debemos reiniciar MariaDB para que los cambios tengan efecto:
```
systemctl restart mariadb
```

Ahora vamos a conectarnos a MariaDB como root para establecer la configuración oportuna:
```
mysql -u root -p
```

Vamos a crear un usuario para WordPress dentro de este servidor, al que estableceremos permisos completos y refrescaremos los mismos:
```
CREATE USER 'wordpress'@'%' identified by 'galicia.wordcamp.org';
GRANT ALL PRIVILEGES ON *.* TO 'wordpress'@'%';
FLUSH PRIVILEGES;
```

A continuación detenemos el proceso de replicación para sincronizarlo con el servidor _Source_:
```
STOP SLAVE;
```

Ejecutaremos la siguiente instrucción con los datos del servidor _Source_, aquí es donde utilizaremos el valor de **File** y **Position**, así como los datos del usuario creado para replicación:
```
CHANGE MASTER TO MASTER_HOST = 'source.peleteiro.eu', MASTER_USER = 'replication', MASTER_PASSWORD = 'your-password', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = 1128;
```

Iniciaremos nuevamente el proceso de replicación:
```
START SLAVE;
```

Con esto la configuración estaría completa, podemos comprobar su estado en todo momento con el siguiente comando:
```
SHOW SLAVE STATUS \G
```


## 4. Instalación y configuración de [HyperDB](https://wordpress.org/plugins/hyperdb/) en WordPress en el servidor web (wordpress.peleteiro.eu)

El proceso de instalación de WordPress no se detallará en esta guía, aunque es conveniente recalcar que en su instalación, **debemos utilizar la base de datos y el usuario que hemos creado en el servidor _Source_**.

Ahora procedemos a descargar el plugin **HyperDB**, para lo que desde el servidor de WordPress ejecutaremos:
```
wget https://downloads.wordpress.org/plugin/hyperdb.1.7.zip
```

A continuación descomprimiremos el ZIP descargado:
```
unzip hyperdb.1.7.zip
```

Entraremos en la carpeta `hyperdb`, y veremos 3 ficheros:
```
-rw-r--r-- 1 root root  15K Aug  7  2012 db-config.php
-rw-r--r-- 1 root root  41K Jul 31  2020 db.php
-rw-r--r-- 1 root root 6.6K Jul 31  2020 readme.txt

```

- El fichero `readme.txt` con instrucciones y documentación sobre el plugin.
- El fichero `db.php`, que será el que reemplace la clase WPDB que trae WordPress por defecto para la gestión de bases de datos.
- El fichero `db-config.php`, donde estableceremos nuestra configuración personalizada.

Ahora vamos con la configuración, para lo que primero editaremos el fichero `wp-config.php` de la instalación de WordPress para comprobar los datos y definir el servidor de _Replica_ (recordemos que el servidor de _Source_ ya debería estar establecido en la instalación de WordPress):
```
vim /var/www/wordpress.peleteiro.eu/wp-config.php
```

Comprobaremos los datos del servidor Source y definiremos la variable del servidor Replica:
```
/** MySQL source hostname */
define( 'DB_HOST', 'source.peleteiro.eu' );

/** MySQL replica hostname */
define( 'DB_REP_HOST', 'replica.peleteiro.eu' );
```

Ahora editaremos el fichero `db-config.php` para establecer la configuración del plugin:
```
vim db-config.php
```

Debemos añadir 2 bloques de base de datos, uno para `Source` y otro para `Replica` de la siguiente forma:
```
$wpdb->add_database(array(
        'host'     => DB_HOST,
        'user'     => DB_USER,
        'password' => DB_PASSWORD,
        'name'     => DB_NAME,
        'write'    => 1,
        'read'     => 1,
));

$wpdb->add_database(array(
        'host'     => DB_REP_HOST,
        'user'     => DB_USER,
        'password' => DB_PASSWORD,
        'name'     => DB_NAME,
        'write'    => 0,
        'read'     => 1,
));
```

**IMPORTANTE**: En la configuración anterior es importante que en el servidor _Replica_ la escritura esté establecida a 0, es decir, que toda escritura vaya contra _Source_, ya que el sistema de replicación que hemos configurado ya llevará cualquier información añadida automáticamente a _Replica_.

Una vez tenemos la configuración hecha, solo nos queda instalar el plugin, para lo que debemos copiar el fichero db.php en el directorio wp-content de nuestra instalación de WordPress:
```
cp db.php /var/www/wordpress.peleteiro.eu/wp-content/
```

Y el fichero db-config.php en el directorio raíz de nuestra instalación de WordPress:
```
cp db-config.php /var/www/wordpress.peleteiro.eu/
```

Llegados a este punto la configuración estaría lista y ya tendríamos un servidor con WordPress instalado (wordpress.peleteiro.eu) conectado a un sistema de bases de datos en alta disponibilidad con 2 servidores replicados (source.peleteiro.eu y replica.peleteiro.eu). 
